const mongoose = require('mongoose');
const DB_URL = require('../db').DB_URL;
const Courts = require('../models/Courts');

const dataCourts = [
    {
        name: 'Agua',
        schedule: [
            { time: '9:00', state: { available: true } },
            { time: '10:00', state: { available: true } },
            { time: '11:00', state: { available: true } },
            { time: '12:00', state: { available: true } },
            { time: '13:00', state: { available: true } },
            { time: '14:00', state: { available: true } },
            { time: '15:00', state: { available: true } },
            { time: '16:00', state: { available: true } },
            { time: '17:00', state: { available: true } },
            { time: '18:00', state: { available: true } },
            { time: '19:00', state: { available: true } },
            { time: '20:00', state: { available: true } },
            { time: '21:00', state: { available: true } },
        ]
    },
    {
        name: 'Fuego',
        schedule: [
            { time: '9:00', state: { available: true } },
            { time: '10:00', state: { available: true } },
            { time: '11:00', state: { available: true } },
            { time: '12:00', state: { available: true } },
            { time: '13:00', state: { available: true } },
            { time: '14:00', state: { available: true } },
            { time: '15:00', state: { available: true } },
            { time: '16:00', state: { available: true } },
            { time: '17:00', state: { available: true } },
            { time: '18:00', state: { available: true } },
            { time: '19:00', state: { available: true } },
            { time: '20:00', state: { available: true } },
            { time: '21:00', state: { available: true } },
        ]
    },
    {
        name: 'Tierra',
        schedule: [
            { time: '9:00', state: { available: true } },
            { time: '10:00', state: { available: true } },
            { time: '11:00', state: { available: true } },
            { time: '12:00', state: { available: true } },
            { time: '13:00', state: { available: true } },
            { time: '14:00', state: { available: true } },
            { time: '15:00', state: { available: true } },
            { time: '16:00', state: { available: true } },
            { time: '17:00', state: { available: true } },
            { time: '18:00', state: { available: true } },
            { time: '19:00', state: { available: true } },
            { time: '20:00', state: { available: true } },
            { time: '21:00', state: { available: true } },
        ]
    },
];


mongoose
    .connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        const courts = await Courts.find();

        if (courts.length) {
            await Courts.collection.drop();
        }
    })
    .catch((err) => {
        console.log(`Error deleting db data ${err}`);
    })
    .then(async () => {
        await Courts.insertMany(dataCourts);
        console.log("Seed saved in DB");
    })
    .catch((err) => {
        console.log(`Error adding data to our db ${err}`);
    })
    .finally(() => mongoose.disconnect());
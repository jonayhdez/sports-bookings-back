const mongoose = require('mongoose');
const DB_URL = require('../db').DB_URL;
const Bookings = require('../models/Bookings');

const dataBookings = [
    {
        date: '08-02-2021',
        court: '60215dcbc81cd9303fd1bc37',
        time: '11:00',
        user: '6018533849999c3970bbc94a'
    },
    {
        date: '08-02-2021',
        court: '60215dcbc81cd9303fd1bc37',
        time: '13:00',
        user: '6018533849999c3970bbc94a'
    },
    {
        date: '08-02-2021',
        court: '60215dcbc81cd9303fd1bc37',
        time: '16:00',
        user: '6018533849999c3970bbc94a'
    },
    {
        date: '08-02-2021',
        court: '60215dcbc81cd9303fd1bc38',
        time: '11:00',
        user: '6018533849999c3970bbc94a'
    },
];


mongoose
    .connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        const bookings = await Bookings.find();

        if (bookings.length) {
            await Bookings.collection.drop();
        }
    })
    .catch((err) => {
        console.log(`Error deleting db data ${err}`);
    })
    .then(async () => {
        await Bookings.insertMany(dataBookings);
        console.log("Seed saved in DB");
    })
    .catch((err) => {
        console.log(`Error adding data to our db ${err}`);
    })
    .finally(() => mongoose.disconnect());
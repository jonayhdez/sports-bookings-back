const Bookings = require('../models/Bookings');
const Courts = require('../models/Courts');

const getBookingsByDate = async (req, res, next) => {
   const day = req.query.day.toString();
   const month = req.query.month.toString();
   const year = req.query.year.toString();
   const calendarDate = `${day}-${month}-${year}`;

    try{
        const bookings = await Bookings.find({
            date: calendarDate
        });

        const courts = await Courts.find();

        const newCourts = [...courts];
        const copyBookings = [...bookings];

        copyBookings.forEach(booking => {
            let foundCourt = newCourts.find(el => el._id == booking.court.toString())
            const index = courts.indexOf(foundCourt);
            const schedule = foundCourt.schedule;
            const foundSchedule = schedule.findIndex(el => el.time === booking.time)
            let newSchedule = { time: booking.time, state: { available: false } };
            schedule.splice(foundSchedule, 1, newSchedule);

            newCourts.splice(index, 1, foundCourt);
        });

        return res.json({ bookings, courts, newCourts });

    }catch (error){
        next(error);
    }
};

const postBooking = async (req, res, next) => {
   const { date, time, court } = req.body;
   const newBooking = new Bookings(
       {
           date,
           time,
           court,
           user: req.user._id
       }
   );

 const savedBooking = await newBooking.save();
 console.log(savedBooking)

};

module.exports = {
    getBookingsByDate,
    postBooking
};
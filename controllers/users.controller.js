const passport = require("passport");

const userRegisterPost = (req, res, next) => {
    passport.authenticate('register',(error, user) =>{
        if (error) next(error)

        req.logIn(user, (error) =>{
            if (error){
                next(error)
            } else {
                return  res.json({ data: req.user })
            }
        });

    })(req, res, next);
};

const userLoginPost = (req, res, next) => {
    passport.authenticate('login', {}, (error, user) => {
        if (error) next(error)

        req.logIn(user, (error) => {
            if (error) {
                next(error)
            } else {
                return res.json({data: req.user})
            }
        });

    })(req, res, next);
};

const userCheckLogin = (req, res, next) => {
  if (req.user){
      return res.json({data: req.user});
  } else {
      req.session.destroy(() => {
          res.clearCookie("connect.sid");
          return res.json({data: null});
      });
  }
};

const userLogout = (req, res, next) => {
    if (req.user) {
        req.logout();
        req.session.destroy(() => {
            res.clearCookie("connect.sid");
            return res.json({data: null});
        });
    } else {
        return res.sendStatus(304);
    }
}

module.exports = {
    userRegisterPost,
    userLoginPost,
    userCheckLogin,
    userLogout
}

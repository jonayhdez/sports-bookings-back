const express = require ('express');
const path = require("path");

const PORT = 3150;
const server = express();
const router = express.Router();

const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const cors = require('cors');

require('dotenv').config();
require('./passport');

const db = require('./db.js');
db.connect();

const userRoutes = require('./routes/users.routes');
const bookingsRoutes = require('./routes/bookings.routes');

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.use(cors({ origin: 'http://localhost:3000' }));

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')));

server.use(
    session({
        secret: 'sportsBookings',
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 360000,
        },
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
);

server.use(passport.initialize());
server.use(passport.session());

server.use('/user', userRoutes);
server.use('/bookings', bookingsRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

server.use((err, req, res, next) => {
    return res.status(err.status || 500).json({
        message: err.message || 'Unexpected error',
        status: err.status || 500,
    });
});

server.use('/', router);
server.listen(PORT,() => console.log(`Server started on http://localhost:${PORT}`));
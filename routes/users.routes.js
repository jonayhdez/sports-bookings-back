const express = require('express');
const usersController = require('../controllers/users.controller');
const router = express.Router();

router.post('/register', usersController.userRegisterPost);
router.post('/login', usersController.userLoginPost);
router.get('/check_login', usersController.userCheckLogin );
router.get('/logout', usersController.userLogout);

module.exports = router;
const express = require('express');
const bookingsController = require('../controllers/bookings.controller');
const router = express.Router();

router.get('/', bookingsController.getBookingsByDate);
router.post('/new', bookingsController.postBooking);

module.exports = router;
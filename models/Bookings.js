const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookingsSchema = new Schema(
    {
        date: { type: String, required: true },
        time: { type: String, required: true },
        court: {
            type: mongoose.Types.ObjectId,
            ref: 'Courts'
        },
        user: {
            type: mongoose.Types.ObjectId,
            ref: 'Users'
        },
    },
    {
        timestamps: true
    }
);

const Bookings = mongoose.model('Bookings', bookingsSchema);

module.exports = Bookings;
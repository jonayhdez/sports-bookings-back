const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courtsSchema = new Schema(
    {
        name: { type: String, required: true },
        schedule: { type: Array, required: true }
    },
    {
        timestamps: true
    }
);

const Courts = mongoose.model('Courts', courtsSchema);
module.exports = Courts;